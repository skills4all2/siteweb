<?php

namespace App\Controller;

use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FrontProfilController extends AbstractController
{
    #[Route('/profil', name: 'app_profil')]
    public function index(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user, [ "from_profil" => true ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // $this->getDoctrine()->getManager()->flush();
            $entityManagerInterface->persist($user);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Profil mis à jour');
            // Redirection vers la home page du site
            return $this->redirectToRoute('app_home');
        }
        return $this->render('front_profil/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
