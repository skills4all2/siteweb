<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(ProduitRepository $produitRepository, UserRepository $userRepository): Response
    {
        $numProduit = count($produitRepository->findAll());
        $numUser = count($userRepository->findAll());
        return $this->render('admin_dashboard/index.html.twig', [
            'numProduit' => $numProduit,
            'numUser'=> $numUser,
        ]);
    }
}
