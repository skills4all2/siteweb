<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_front_produit')]
    public function index(ProduitRepository $produitRepository, Request $request): Response
    {
        $produit = null;
        $search = $request->request->get("search");
        if(!is_null($search)){
            $produits = $produitRepository->search($search);
        }else{
            $produits = $produitRepository->findAll();
        }
        return $this->render('front_produit/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    #[Route('/produit/{id}', name: 'app_front_produit_detail')]
    public function detail($id, ProduitRepository $produitRepository): Response{
        $produit = $produitRepository->find($id);
        return $this->render('front_produit/detail.html.twig', [
            'produit' => $produit,
        ]);
    }
}
