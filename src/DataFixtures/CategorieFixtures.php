<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CategorieFixtures extends Fixture
{
    public const INFORMATIQUE = "Informatique";
    public const ELECTROMENAGER = "ELectromenager";
    public const VETEMENTS = "Vetements";
    //
    public function load(ObjectManager $manager): void
    {
        $categorie = new Categorie();
        $categorie->setName('Informatique');
        $manager->persist($categorie);
        $this->addReference(self::INFORMATIQUE, $categorie);

        $categorie = new Categorie();
        $categorie->setName('Electroménager');
        $manager->persist($categorie);
        $this->addReference(self::ELECTROMENAGER, $categorie);

        $categorie = new Categorie();
        $categorie->setName('Vêtements');
        $manager->persist($categorie);
        $this->addReference(self::VETEMENTS, $categorie);

        $manager->flush();
    }
}
