<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('jean.test@test.com');
        $user->setFirstName('Jean');
        $user->setLastname('Test');
        $user->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $user->setCivilite('Mr');
        $user->setPassword($this->encoder->hashPassword($user, 'password'));
        $user->setIsVerified(true);
        $manager->persist($user);

        $manager->flush();
    }
}
