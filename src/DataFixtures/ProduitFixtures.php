<?php

namespace App\DataFixtures;

use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProduitFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $produit = new Produit();
        $produit->setName('MacBook Pro');
        $produit->setReference("123456");
        $produit->setCategorie($this->getReference(CategorieFixtures::INFORMATIQUE));
        $produit->setMadeInFrance(false);
        $produit->setNouveaute(true);
        $manager->persist($produit);

        $produit = new Produit();
        $produit->setName('Machine à laver 10Kg');
        $produit->setReference("ML5678");
        $produit->setMadeInFrance(true);
        $produit->setNouveaute(false);
        $produit->setCategorie($this->getReference(CategorieFixtures::ELECTROMENAGER));
        $manager->persist($produit);

        $manager->flush();
    }
}
