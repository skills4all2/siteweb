<?php

namespace App\Form;

use App\Entity\Couleur;
use App\Entity\Produit;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ["label"=> "Nom du produit", "constraints"=> [new NotBlank(["message"=> "Le nom du produit est obligatoire"])]])
            ->add('accroche', TextType::class, ["label"=> "Accroche du produit", "required"=> false])
            ->add('reference', TextType::class, ["label"=> "Référence du produit"])
            ->add('description', CKEditorType::class, ["label"=> "Description du produit"])
            ->add('dimension', TextType::class, ["label"=> "Dimension du produit", "required"=> false])
            ->add('colisage', TextType::class, ["label"=> "Colisage du produit", "required"=> false])
            ->add('poids', TextType::class, ["label"=> "Poids du produit", "required"=> false])
            ->add('madeInFrance', CheckboxType::class, ["label"=> "Fabriqué en France", "required"=> false])
            ->add('nouveaute', CheckboxType::class, ["label"=> "Nouveauté", "required"=> false])
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'name',
                'label'=> "Catégorie du produit"
            ])
            ->add('couleurs', EntityType::class, [
                'class' => Couleur::class,
                'choice_label' => 'name',
                'multiple' => true,
                'label'=> "Couleurs du produit",
                "required"=> false
            ])
            ->add('imageFile', FileType::class, ["label"=> "Image du produit", "required"=> false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
