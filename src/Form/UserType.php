<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Avatar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('civilite', ChoiceType::class, [
            "label" => "Civilité", 
            "choices" => ["Monsieur" => "M.", "Madame" => "Mme"],
            "required"=>true,
            ])
        ->add('firstname', TextType::class, ["required"=>true, "label"=>"Prénom"])
        ->add('lastname', TextType::class, ["required"=>true, "label"=>"Prénom"])
        ->add('email', EmailType::class, ["required"=>true, "label"=>"Email"])
        ->add('avatar', AvatarType::class, [
            "label" => false,
        ])
        ;
        if(!$options["from_profil"]){
            $builder
            ->add('roles')
            ->add('password')
            ->add('isVerified');
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'from_profil' => false,
        ]);
    }
}
